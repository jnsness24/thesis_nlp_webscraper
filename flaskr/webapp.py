from flask import Flask, render_template, jsonify, request
import json
from collections import defaultdict
import analysis.analysis_controller as a_c


import en_core_web_sm as en
import de_core_news_sm as de
from spacy import displacy
from elasticsearch import Elasticsearch
import subprocess
from pathlib import Path

app = Flask(__name__)
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

def retrieve_source_text(id):
    es = Elasticsearch('5qrfvytdf2:8l2r3918kx@elm-533263851.eu-west-1.bonsaisearch.net',scheme='https',port=443)

    res = es.get(index='web-events', doc_type='web-items', id=id)
    source_text = res['_source']['texts']['source_texts']
    language = res['_source']['meta']['language']

    return (source_text,language)


@app.route('/')
def hello_world():
   return "Hello World 2"

@app.route('/displacy-endpoint/<id>')
def display(id):
    print("retrieved")
    source_text, language = retrieve_source_text(id)
    if language in ['de','de-DE']:
        nlp = de.load()
    else:
        if language in ['en','en-US']:
            nlp = en.load()

    doc1 = nlp(source_text)
    html = displacy.render(doc1, style='dep', page=True)

    return html

@app.route('/crawl-endpoint/<urlstring>')
def scrape_this(urlstring):
    urlstring = "https://www."+urlstring+"/robots.txt"
    data_folder = Path('../thesis_example')
    print(data_folder)
    command = "scrapy crawl sitemap_spider -a url={} -L WARNING".format(urlstring)
    print(command)
    # result = subprocess.run(command, cwd=data_folder, stdout= subprocess.PIPE)
    def cmd_output():
        proc = subprocess.Popen(command,shell=True,cwd=data_folder,stdout=subprocess.PIPE)
        for line in iter(proc.stdout.readline,''):
            yield str(line.rstrip()) + "</br>"
    return Flask.Response(cmd_output(), mimetype='text/html')


def process_form_data(form_data):
    sliced_data = form_data.split('%')
    print(sliced_data)

@app.route('/config/', methods=['GET', 'POST'])
def config():
    with open('../analysis/config.json','r') as f:
        json_file = json.load(f)


    if request.method == 'POST':
        d = defaultdict(dict)
        requested_data = request.form.to_dict()
        for element, value in requested_data.items():
            # transform http text data to fitting datatypes -
            try:
                value = json.loads(value)
            except ValueError:
                pass

            parent, child = element.split('-')
            d[parent][child] = value

            with open('../analysis/config.json','w') as f:
                json.dump(d,f)

    template = render_template('base.html', json_file=json_file)

    return template

@app.route('/start_analyzer')
def start_analyzer():

    a_c.run(use_cached=False)
    return "<div>Analysis finished</div><a href='http://127.0.0.1:1337/config/'>back to Configurator</a>"


if __name__ == '__main__':
   app.run(port=1337, debug=True)