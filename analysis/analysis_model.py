from elasticsearch import Elasticsearch
import pickle
import time

class Es_sentences:
    """
    Connection to Document Storage. After a remote connection and query is done, it is saved to disk for offline usage
    local/offline access is only a pickle.load of the JSON construct

    Remote Connection can be influenced by query in the doc
    After that the lists are "set and sorted". Set garantuees that no dublicate sentences are inside a sorted list
    Every List is saved for later different usages
    """
    def __init__(self):

        self.english_cleaned_list = []
        self.cleaned_list = []
        self.english_source_list = []
        self.source_list = []
        self.english_noun_chunks_list = []
        self.noun_chunks_list = []
        self.filename = "pickle_file"

    def get_data_from_local(self):
        f = open(self.filename, 'rb')
        tmp_dict = pickle.load(f)
        f.close()

        self.__dict__.update(tmp_dict)

    def _save(self):
        f = open(self.filename, 'wb')
        pickle.dump(self.__dict__, f, 2)
        f.close()


    def get_data_from_server(self):

        es = Elasticsearch('5qrfvytdf2:8l2r3918kx@elm-533263851.eu-west-1.bonsaisearch.net',scheme='https',port=443)

        # doc = {
        #     'size' : 10000,
        #     'sort' : {
        #         'texts.source_texts.keyword' : {
        #             'order' : 'asc'
        #         }
        #     },
        #     'query' : {
        #         'exists' : {
        #             "field" : "texts.enriched_dep_subverb_texts"
        #         }
        #     }
        # }
        doc = {
            'size' : 10000,
            'sort' : {
                "texts.source_texts.keyword" : {
                    "order" : "asc"
                }
            },
            'query' : {
                'match_all' : {
                }
            }
        }


        res = es.search(index='web-events',doc_type='web-items', body=doc)

        for item in res['hits']['hits']:
            self.cleaned_list.append(item['_source']['texts']['cleaned_texts'])
            self.source_list.append(item['_source']['texts']['source_texts'])
            self.noun_chunks_list.append(item['_source']['texts']['noun_chunks'])

        # generate cleaned and source lists, that keep the sorting
        self.unique_cleaned_list = list(sorted(set(self.cleaned_list), key=lambda x: self.cleaned_list.index(x)))
        self.unique_source_list = list(sorted(set(self.source_list), key=lambda x: self.source_list.index(x)))
        self.unique_noun_chunks_lists = list(sorted(set(self.noun_chunks_list), key=lambda x: self.noun_chunks_list.index(x)))

        # wanna have list of lists? just transform it via - [d.split() for d in list]

        self._save()





