from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader
import textacy

class Corpus_Statistics(Corpus_Converter,Config_Loader):

    def __init__(self,corpus,corpus_name, save_string = None, config=None):
        Corpus_Converter.__init__(self, corpus, corpus_name,save_string)
        Config_Loader.__init__(self,config)

    def process(self):
        if(self.check_process() == False):
            return
        ts = textacy.TextStats(super().retrieve_textacy_doc())
        print(ts.basic_counts)
        print(ts.readability_stats)


