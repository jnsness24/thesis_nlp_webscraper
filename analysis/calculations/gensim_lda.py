from analysis.corpus_converter import Corpus_Converter
from analysis.coherence_calculator import Coherence_Calculator
from analysis.config_loader import Config_Loader
from pprint import pprint
from gensim.models import LdaModel
from matplotlib import pyplot as plt
import pyLDAvis.gensim
import time
import os

class Gensim_Lda(Corpus_Converter,Coherence_Calculator,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self,config)

    def _print_pyLDAvis(self, model, gensim_doc, gensim_id2word):
        try:
            vis_data = pyLDAvis.gensim.prepare(model, gensim_doc, gensim_id2word)
            print("saving pyLDAvis to html file....")
            pyLDAvis.save_html(vis_data, os.path.join(self.save_string, "pyLDAvis_data_{0}_{1}.html".format(time.time(),
                                                                                                            self.corpus_name)))
        except AssertionError:
            print("Assertion-Error - Topicamount too small for calculation")

    def process(self):
        if(self.check_process() == False):
            return



        pprint("-x-x-x- Generating LDA Calculation -x-x-x-")
        gensim_doc = super().retrieve_gensim_doc()
        gensim_bigram = super().retrieve_gensim_bigram()
        gensim_id2word = super().retrieve_gensim_dictionary()


        num_topics = self.config['num_topics']
        show_viz = self.config['show_viz']

        try:
            if num_topics == 0:
                num_topics = super()._discover_optimal_topic_amount(20 ,LdaModel,gensim_doc,gensim_bigram,gensim_id2word)

            ldamodel = LdaModel(corpus=gensim_doc ,num_topics=num_topics,id2word=gensim_id2word)
            pprint(ldamodel.show_topics(formatted=False))
            pprint("Coherence of this model: {}".format(super()._get_coherence(ldamodel,gensim_doc)))

            if show_viz == True:
                self._print_pyLDAvis(ldamodel,gensim_doc,gensim_id2word)
        except ValueError:
            print("no terms inside collection")