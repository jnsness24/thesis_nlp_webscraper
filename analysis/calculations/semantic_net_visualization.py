from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader
import textacy
import textacy.keyterms
import os
import time
from matplotlib import pyplot as plt


class Semantic_Net_Visualization(Corpus_Converter,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self,config)


    def process(self):
        if(self.check_process() == False):
            return

        cropping_amount = self.config['cropping_amount']

        textacy_cropped_net = super().retrieve_textacy_cropped_net(cropping_amount)
        textacy_cropped_bow = super().retrieve_textacy_cropped_bow(cropping_amount)

        textacy.viz.network.draw_semantic_network(textacy_cropped_net, node_weights=textacy_cropped_bow, draw_nodes=True)
        plt.savefig(os.path.join(self.save_string,"semantic_net_visualisation_{0}_{1}.svg".format(time.time(),self.corpus_name)),format='svg')