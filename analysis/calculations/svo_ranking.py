from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader
from pprint import pprint
import textacy
import textacy.extract as te

class Svo_Ranking(Corpus_Converter,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self,config)


    def process(self):
        if(self.check_process() == False):
            return

        textacy_doc = super().retrieve_textacy_doc()

        pprint("-x-x-x- Print the top ngrams in Korpus -x-x-x-")
        extracted_svo = te.subject_verb_object_triples(textacy_doc)
        print(extracted_svo)
        pprint(list(extracted_svo))