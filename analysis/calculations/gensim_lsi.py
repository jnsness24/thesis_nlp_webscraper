from analysis.corpus_converter import Corpus_Converter
from analysis.coherence_calculator import Coherence_Calculator
from analysis.config_loader import Config_Loader
from pprint import pprint
from gensim.models import LsiModel

class Gensim_Lsi(Corpus_Converter,Coherence_Calculator,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string = None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self,config)


    def process(self):
        if(self.check_process() == False):
            return

        pprint("-x-x-x- Generating LSI Calculation -x-x-x-")
        gensim_doc = super().retrieve_gensim_doc()
        gensim_bigram = super().retrieve_gensim_bigram()
        gensim_id2word = super().retrieve_gensim_dictionary()

        num_topics = self.config['num_topics']

        try:
            if num_topics == 0:
                num_topics = super()._discover_optimal_topic_amount(20 ,LsiModel,gensim_doc,gensim_bigram,gensim_id2word)

            lsimodel = LsiModel(corpus=gensim_doc ,num_topics=num_topics,id2word=gensim_id2word)
            pprint(lsimodel.show_topics(formatted=False))
            pprint("Coherence of this model: {}".format(super()._get_coherence(lsimodel,gensim_doc)))
        except ValueError:
            print("no terms inside collection")