from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader

from pprint import pprint
import textacy
import textacy.keyterms

class Semantic_Net_Nodes_Ranking(Corpus_Converter,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string = None,config= None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self,config)


    def process(self):
        if(self.check_process() == False):
            return

        textacy_net = super().retrieve_textacy_net()

        k = self.config['k']
        c = self.config['c']
        alpha = self.config['alpha']

        pprint("-x-x-x- Print the ranked nodes, calculated by the semantic net -x-x-x-")
        extracted_coverage_from_sn = textacy.keyterms.rank_nodes_by_bestcoverage(textacy_net, k=k, c=c, alpha=alpha)
        pprint(extracted_coverage_from_sn)