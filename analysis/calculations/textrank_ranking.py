from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader
from pprint import pprint
import textacy

class Textrank_Ranking(Corpus_Converter,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self,config)


    def process(self):
        if(self.check_process() == False):
            return

        textacy_doc = super().retrieve_textacy_doc()

        normalize = self.config['normalize']
        n_keyterms = self.config['n_keyterms']

        pprint("-x-x-x- Print the ranked keyterms, calculated by TextRank algorithm -x-x-x-")
        extracted_key_terms_textrank = textacy.keyterms.textrank(textacy_doc, normalize=normalize, n_keyterms=n_keyterms)
        pprint(extracted_key_terms_textrank)