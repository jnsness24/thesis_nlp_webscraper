from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader
from pprint import pprint
from matplotlib import pyplot as plt
import os
import time

class Topic_Modelling(Corpus_Converter,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self, config)

    def process(self):
        if(self.check_process() == False):
            return

        textacy_model = super().retrieve_textacy_model()
        textacy_vectorizer = super().retrieve_textacy_vectorizer()
        textacy_doc_term_matrix = super().retrieve_textacy_doc_term_matrix()

        topics = self.config['topics']
        n_terms = self.config['n_terms']
        show_viz = self.config['show_viz']



        pprint("-x-x-x- Print topics from topic modelling -x-x-x-")
        for topic_idx, top_terms in textacy_model.top_topic_terms(textacy_vectorizer.id_to_term, top_n=10):
            pprint(('topic', topic_idx, ':', '   '.join(top_terms)))

        if show_viz == True:
            textacy_model.termite_plot(textacy_doc_term_matrix, textacy_vectorizer.id_to_term, topics=topics, n_terms=n_terms)
            plt.savefig(os.path.join(self.save_string, "tm_termite_plot_{0}_{1}.svg".format(time.time(), self.corpus_name)),
                    format='svg')

