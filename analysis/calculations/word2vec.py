from analysis.corpus_converter import Corpus_Converter
from analysis.coherence_calculator import Coherence_Calculator
from analysis.config_loader import Config_Loader
from pprint import pprint
from matplotlib import pyplot as plt
from sklearn.manifold import TSNE
import os
import time
from gensim.models import Word2Vec
import multiprocessing

class Word_2_Vec(Corpus_Converter,Coherence_Calculator,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self, config)



    def _plot_tsne(self,model):
        # Creates and TSNE model and plots it
        labels = []
        tokens = []

        for word in model.wv.vocab:
            tokens.append(model.wv[word])
            labels.append(word)

        tsne_model = TSNE(perplexity=40, n_components=2, init='pca', n_iter=2500, random_state=23)
        new_values = tsne_model.fit_transform(tokens)

        x = []
        y = []
        for value in new_values:
            x.append(value[0])
            y.append(value[1])

        plt.figure(figsize=(16, 16))
        for i in range(len(x)):
            plt.scatter(x[i], y[i])
            plt.annotate(labels[i],
                         xy=(x[i], y[i]),
                         xytext=(5, 2),
                         textcoords='offset points',
                         ha='right',
                         va='bottom')
        plt.savefig(os.path.join(self.save_string,"w2v_data_{0}_{1}.svg".format(time.time(),self.corpus_name)),format='svg')

    def process(self):
        if(self.check_process() == False):
            return

        gensim_bigram = super().retrieve_gensim_bigram()

        min_count = self.config['min_count']
        window = self.config['window']
        size = self.config['size']
        similar_to = self.config['similar_to']
        show_viz = self.config['show_viz']


        model = Word2Vec(gensim_bigram, min_count=min_count,window=window, workers=multiprocessing.cpu_count(), size=size)
        if show_viz == True:
            self._plot_tsne(model)

        #todo try error with KeyError
        if similar_to is not None:
            try:
                pprint("Most Similar to {} is :".format(similar_to))
                pprint(model.wv.most_similar(similar_to))
            except KeyError:
                pprint("Word {} is not in vocabulary".format(similar_to))