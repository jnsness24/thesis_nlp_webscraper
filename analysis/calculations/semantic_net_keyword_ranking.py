from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader
from pprint import pprint
import textacy.keyterms

class Semantic_Net_Keyword_Ranking(Corpus_Converter,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self,config)


    def process(self):
        if(self.check_process() == False):
            return

        textacy_doc = super().retrieve_textacy_doc()

        normalize = self.config['normalize']
        window_width = self.config['window_width']
        edge_weighting = self.config['edge_weighting']
        ranking_algo = self.config['ranking_algo']
        join_key_words = self.config['join_key_words']
        n_keyterms = self.config['n_keyterms']



        print("-x-x-x- Print the most relevant keyterms, calculated via semantic net -x-x-x-")
        extracted_keyterms_from_sn = textacy.keyterms.key_terms_from_semantic_network(textacy_doc,
                                                                                      normalize=normalize,
                                                                                      window_width=window_width,
                                                                                      edge_weighting=edge_weighting,
                                                                                      ranking_algo=ranking_algo,
                                                                                      join_key_words=join_key_words,
                                                                                      n_keyterms=n_keyterms)
        pprint(extracted_keyterms_from_sn)