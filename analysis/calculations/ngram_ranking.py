from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader

from pprint import pprint
import textacy

class Ngram_Ranking(Corpus_Converter,Config_Loader):
    def __init__(self,corpus,corpus_name, save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self,config)

    def process(self):
        if(self.check_process() == False):
            return

        ngram = self.config['ngram']
        filter_stops = self.config['filter_stops']
        filter_punct = self.config['filter_punct']
        filter_nums = self.config['filter_nums']
        min_freq = self.config['min_freq']


        textacy_doc = super().retrieve_textacy_doc()

        pprint("-x-x-x- Print the top ngrams in Korpus -x-x-x-")
        extracted_ngram = textacy.extract.ngrams(textacy_doc, ngram, filter_stops=filter_stops, filter_punct=filter_punct,
                                                 filter_nums=filter_nums, min_freq=min_freq)
        list_engram = list(extracted_ngram)
        print(list_engram[:30])

        #pprint(list(extracted_ngram))