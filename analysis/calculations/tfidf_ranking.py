from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader

from analysis.coherence_calculator import Coherence_Calculator
import gensim
from pprint import pprint

class Tfidf_Ranking(Corpus_Converter,Coherence_Calculator,Config_Loader):
    def __init__(self,corpus,corpus_name,save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self, config)

    def process(self):
        if(self.check_process() == False):
            return

        cutoff = self.config['cutoff']

        gensim_doc = super().retrieve_gensim_doc()
        gensim_id2word = super().retrieve_gensim_dictionary()

        tfidf = gensim.models.tfidfmodel.TfidfModel(gensim_doc)
        corpus_tfidf = tfidf[gensim_doc]

        tfidf_weights = {gensim_id2word.get(id): value
                         for doc in corpus_tfidf
                         for id, value in doc}
        sorted_tfidf_weights = sorted(tfidf_weights.items(), key=lambda w: w[1], reverse=True)

        pprint("TFIDF Sorted List Top {}".format(cutoff))
        pprint(sorted_tfidf_weights[:cutoff])