from analysis.corpus_converter import Corpus_Converter
from analysis.config_loader import Config_Loader
from pprint import pprint
import textacy

class Sgrank_Ranking(Corpus_Converter, Config_Loader):
    def __init__(self,corpus,corpus_name,save_string=None,config=None):
        Corpus_Converter.__init__(self,corpus,corpus_name,save_string)
        Config_Loader.__init__(self,config)

    def process(self):
        if(self.check_process() == False):
            return

        textacy_doc = super().retrieve_textacy_doc()

        ngrams = self.config['ngrams']
        normalize = self.config['normalize']
        window_width = self.config['window_width']
        n_keyterms = self.config['n_keyterms']

        pprint("-x-x-x- Print the ranked keyterms, calculated by SGRANK algorithm -x-x-x-")
        extracted_key_terms_sgrank = textacy.keyterms.sgrank(textacy_doc, ngrams=ngrams,
                                                             normalize=normalize,
                                                             window_width=window_width, n_keyterms=n_keyterms)
        pprint(extracted_key_terms_sgrank)