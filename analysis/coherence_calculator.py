from gensim.models import CoherenceModel


class Coherence_Calculator():

    def _discover_optimal_topic_amount(self,maxValue, model, gensim_corpus, bigram_texts,id2word):

        c_v = []
        limit = maxValue
        for num_topics in range(1, limit):
            lm = model(corpus=gensim_corpus, num_topics=num_topics, id2word=id2word)
            cm = CoherenceModel(model=lm, texts=bigram_texts, dictionary=id2word, coherence='u_mass')
            c_v.append(cm.get_coherence())
        maximum = max(c_v)
        index = c_v.index(maximum)
        print("optimal topic amount is {}".format(index+1))
        return index + 1

    def _get_coherence(self,model,corpus):
        return CoherenceModel(model=model,corpus=corpus,coherence='u_mass').get_coherence()