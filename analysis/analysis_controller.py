import analysis.analysis_model as receiver
from analysis.calculations import *
from analysis.corpus_converter import Corpus_Converter
import json
from nltk.corpus import gutenberg
from gensim.parsing.preprocessing import preprocess_string
import os
import time
import datetime
import sys


def _prepare_save_string(corpus_name):
    save_string = "../analysis/results/result_{:%Y-%b-%d_%H_%M_%S}_{}".format(datetime.datetime.now(), corpus_name)
    os.mkdir(save_string)
    log_file = open(os.path.join(save_string, "logfile_{}.txt".format(time.time())), 'w')
    sys.stdout = log_file
    return save_string



def _call_every_analysis_method(corpus, corpus_name):
    """
    1. instantiates every subclass of Corpus Converter (you could say "every calculation in calculations package")
    2. gives the fitting config-json of whole-config-json to the instantiated object (classname is the same as first level of json)
    3. calls the process method of the instantiated object (every object has one)

    :param model_object:
    :return:
    """

    with open('../analysis/config.json') as file:
        json_file = json.load(file)


    save_string = _prepare_save_string(corpus_name)

    for obj in Corpus_Converter.__subclasses__():
        classname = obj.__name__
        config = json_file[classname]
        obj_instance = obj(corpus,corpus_name,save_string,config)
        obj_instance.process()

    print("the original list was: {}".format(corpus_name))



def test_run(corpus, name):
    """
    Test run - works with reference corpusses from e.g. NLTK
    uses the same analysis methodes as RUN
    :param corpus:
    :param name:
    :return:
    """
    validation_test_sentences = corpus

    validation_test_preproc = []

    for elements in validation_test_sentences:
        test = ' '.join(elements)
        test = preprocess_string(test)
        validation_test_preproc.append(test)

    splitted_validation_test_preproc = [' '.join(x) for x in validation_test_preproc]

    _call_every_analysis_method(splitted_validation_test_preproc, name)

def run(use_cached=False):
    """
    actual start-method for analysis.
    First it catches the data from document storage (or the cached one from harddisk)
    Every list is hold in a dictionary and every method is called (by every list)

    Takes a long time!!!
    :param use_cached:
    :return:
    """
    es_data = receiver.Es_sentences()
    if use_cached == True:
        es_data.get_data_from_local()
    else:
        es_data.get_data_from_server()

    german_list_names = {
        "source": es_data.source_list,
        "unique source": es_data.unique_source_list,
        "cleaned": es_data.cleaned_list,
        "unique cleaned": es_data.unique_cleaned_list,
        "noun chunks": es_data.noun_chunks_list,
        "unique noun chunks": es_data.unique_noun_chunks_lists}

    for key,value in german_list_names.items():
        corpus_name = key
        corpus = value

        _call_every_analysis_method(corpus,corpus_name)



# corpus = gutenberg.sents("melville-moby_dick.txt")
# test_run(corpus,"moby-dick")
#run(use_cached=False)


if __name__ == '__main__':
   run(use_cached=False)