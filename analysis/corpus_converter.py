import textacy
from textacy import text_utils
import gensim
from gensim.corpora import Dictionary

en = textacy.load_spacy('en_core_web_sm')
de = textacy.load_spacy('de_core_news_sm')

#Todo there are some configurations left in the instantiation of the corpusses. currently the config file does not reach them - they are, indeed, not that critical, but it would be nice to control them
class Corpus_Converter():
    def __init__(self,corpus,corpus_name,save_string=None):
        self.corpus = corpus
        self.corpus_name = corpus_name
        if save_string != None:
            self.save_string = save_string


    def _join_text(self, corpus):
        return ' '.join(corpus)

    def _split_text(self,corpus):
        return [d.split() for d in corpus]

    def _crop_semantic_net(self,net,bow,cut_off):
        _remove_node_list = []
        _sorted_by_degree = sorted(net.degree, key=lambda x: x[1], reverse=True)
        _cut_top_sorted_degree = dict(_sorted_by_degree[:cut_off])

        # Use Top 100 Ranked Items (in Degree) of Semantic Net
        for node,degree in net.degree(weight='weight'):
            if node not in _cut_top_sorted_degree.keys():
                _remove_node_list.append(node)
                #delete the same instance in the bow list - this is used for weight labeling and it has to be the same as semantic network
                del bow[node]

        net.remove_nodes_from(_remove_node_list)
        return net,bow

    def _generate_textacy_docs(self,cropping_amount = 100):
        _textacy_cleaned_splittet_corpus = textacy.preprocess.preprocess_text(self._join_text(self.corpus), fix_unicode=True, lowercase=True,
                                                                              no_urls=True, no_emails=True, no_phone_numbers=True,
                                                                              no_numbers=True, no_currency_symbols=True,
                                                                              no_punct=True, no_contractions=True, no_accents=True)

        _detected_lang = text_utils.detect_language(_textacy_cleaned_splittet_corpus)
        _language_pack = de

        if _detected_lang == 'de':
            _language_pack = de
        else:
            if _detected_lang == 'en':
                _language_pack = en

        _textacy_doc = textacy.Doc(_textacy_cleaned_splittet_corpus,lang=_language_pack)
        _textacy_terms_list = _textacy_doc.tokenized_text
        _textacy_vectorizer = textacy.Vectorizer(tf_type='linear', apply_idf=True,idf_type='smooth')
        _textacy_doc_term_matrix = _textacy_vectorizer.fit_transform(_textacy_terms_list)
        _textacy_model = textacy.tm.TopicModel('lda',n_components=5)
        _textacy_model.fit(_textacy_doc_term_matrix)
        _textacy_bow = _textacy_doc.to_bag_of_words(weighting='count', as_strings=True,normalize=None)
        _textacy_net = _textacy_doc.to_semantic_network(nodes='words',window_width=10,normalize=None)
        _cropped_textacy_net, _cropped_textacy_bow = self._crop_semantic_net(_textacy_net,_textacy_bow,cropping_amount)



        self.textacy_doc = _textacy_doc
        self.textacy_doc_term_matrix = _textacy_doc_term_matrix
        self.textacy_model = _textacy_model
        self.textacy_vectorizer = _textacy_vectorizer
        self.textacy_bow = _textacy_bow
        self.textacy_net = _textacy_net
        self.cropped_textacy_bow = _cropped_textacy_bow
        self.cropped_textacy_net = _cropped_textacy_net


        #Todo Dictionary with fixed names would be smarter - in this scenario, getter needs to know the position of corpus
        docs = [_textacy_doc, _textacy_doc_term_matrix, _textacy_model, _textacy_bow, _textacy_net, _cropped_textacy_bow, _cropped_textacy_net, _textacy_vectorizer]

        return docs

    def _generate_gensim_docs(self):
        _splitted_text = self._split_text(self.corpus)
        _bigram_list = gensim.models.Phrases(_splitted_text)
        _gensim_bigram_text = [_bigram_list[line] for line in _splitted_text]
        _gensim_dictionary = Dictionary(_gensim_bigram_text)
        _gensim_dictionary.filter_extremes(no_below=50,no_above=0.8,keep_n=100000)
        _gensim_doc = [_gensim_dictionary.doc2bow(text) for text in _gensim_bigram_text]

        self.gensim_bigram_text = _gensim_bigram_text
        self.gensim_dictionary = _gensim_dictionary
        self.gensim_doc = _gensim_doc


        #Todo Dictionary with fixed names would be smarter - in this scenario, getter needs to know the position of corpus
        docs = [_gensim_bigram_text,_gensim_dictionary,_gensim_doc]

        return docs




#########################################
    # you can call them "getter" from now on - every method gets his preferred corpus
    # the try - except construct helps for caching - so the calculations may not be repeated

    def retrieve_textacy_doc(self):
        # "easier to ask for forgiveness than permission" - EAFP
        try:
            return self.textacy_doc
        except AttributeError:
            return self._generate_textacy_docs()[0]

    def retrieve_textacy_net(self):
        # "easier to ask for forgiveness than permission" - EAFP
        try:
            return self.textacy_net
        except AttributeError:
            return self._generate_textacy_docs()[4]

    def retrieve_textacy_cropped_net(self,cropping_amount):
        # "easier to ask for forgiveness than permission" - EAFP
        try:
            return self.cropped_textacy_net
        except AttributeError:
            return self._generate_textacy_docs(cropping_amount)[6]

    def retrieve_textacy_cropped_bow(self,cropping_amount):
        # "easier to ask for forgiveness than permission" - EAFP
        try:
            return self.cropped_textacy_bow
        except AttributeError:
            return self._generate_textacy_docs(cropping_amount)[5]

    def retrieve_textacy_model(self):
        # "easier to ask for forgiveness than permission" - EAFP
        try:
            return self.textacy_model
        except AttributeError:
            return self._generate_textacy_docs()[2]

    def retrieve_textacy_vectorizer(self):
        # "easier to ask for forgiveness than permission" - EAFP
        try:
            return self.textacy_vectorizer
        except AttributeError:
            return self._generate_textacy_docs()[7]

    def retrieve_textacy_doc_term_matrix(self):
        # "easier to ask for forgiveness than permission" - EAFP
        try:
            return self.textacy_doc_term_matrix
        except AttributeError:
            return self._generate_textacy_docs()[1]

    def retrieve_gensim_doc(self):
        # "easier to ask for forgiveness than permission" - EAFP
        try:
            return self.gensim_doc
        except AttributeError:
            return self._generate_gensim_docs()[2]

    def retrieve_gensim_dictionary(self):
        try:
            return self.gensim_dictionary
        except AttributeError:
            return self._generate_gensim_docs()[1]

    def retrieve_gensim_bigram(self):
        try:
            return self.gensim_bigram_text
        except AttributeError:
            return self._generate_gensim_docs()[0]


