# Thesis NLP Webscraper
This software toolkit has been created for the two purpoes of information retrieval and information extraction

It can extract latent textinformation out of websites for the technolgy forecasting

webpages can be scraped/crawled and will be saved to an elasticsearch instance
Natural language processing analytics can be done afterwords

## Getting Started
### Prerequisites

Python(3) and PIP should be running on your system
Elasticsearch with Kibana (ELK without the L) should be running on a server

You can use Bonsai Elasticsearch in a heroku cluster (I used that too)

https://elements.heroku.com/addons/bonsai


Get the Code

https://bitbucket.org/jnsness24/thesis_nlp_webscraper.git

### Installing
Scrapy 
```bash
pip install Scrapy
scrapy startproject thesis_example
```
copy the files from Repository in the appropriate folder

Spacy (with english and german language models)
```bash
pip install -U spacy
python -m spacy download en_core_web_sm
python -m spacy download de_core_news_sm
```

Textacy
```bash
pip install textacy[all]
```

Gensim
```bash
pip install --upgrade gensim
```

Elasticsearch driver
```bash
pip install elasticsearch
```

NLTK
```bash
pip install -U nltk
```

Flask
```bash
pip install Flask
```

## Use the Software
### Scrape/Crawl Website
change url in uploadpipeline to your own elasticsearch cluster
* delete_index: should go to delete API. IF you want to delete the index after upload. If you want to keep it --> just comment out the method-call
* bulk: should go to the bulk API 

Go to the folder \thesis_example
write this in terminal

````bash
scrapy crawl sitemap_spider -a url=https://www.test.de/robots.txt -s CLOSESPIDER_ITEMCOUNT=9500
````

url accepts links to robots.txt (with sitemap in it) or direct sitemap.xml link
closespider argument is needed with heroku because of the free limits - can be deleted with private hosting

### WebServer
Go to the flask Folder and open webapp.py
Change Elasticsearch Link to your Server

Change Port to your favorite Port

```python
if __name__ == '__main__':
   app.run(port=1337, debug=True)
```

Run the webapp.py in folder flaskr

### Kibana
After Indexing is complete go to your kibana
Management --> Index Patterns --> Create Index Pattern

edit _id field --> 
* Url Template --> {url-webserver}/displacy-endpoint/{{value}}
* Label Template --> {{value}} - View Dependancy

### Analysis
With a running webserver just call the {url webserver}/config Endpoint
here you can edit the config and start the analysis - it always takes the current data from Elasticsearch

