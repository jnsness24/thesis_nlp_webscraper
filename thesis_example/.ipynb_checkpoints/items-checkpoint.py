# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Item(scrapy.Item):
    # define the fields for your item here like:
    pTag = scrapy.Field()
    h1Tag = scrapy.Field()
    h2Tag = scrapy.Field()
    h3Tag = scrapy.Field()

    # Housekeeping
    #project = scrapy.Field()
    #spider = scrapy.Field()
    #server = scrapy.Field()
    #date = scrapy.Field()

    pass
