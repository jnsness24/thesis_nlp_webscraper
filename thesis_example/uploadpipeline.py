import requests

class UploadPipeline():
    """
    Pipeline for finishing the process
    after all Items are processed - here it will be automatically uploaded to document storage
    old content will be deleted before that
    """

    def __init__(self):
        pass
        # self.path_to_file = "shell_scripts\\renew_web-events_index.sh"

    def close_spider(self,spider):


        def delete_index():
            url = 'https://5qrfvytdf2:8l2r3918kx@elm-533263851.eu-west-1.bonsaisearch.net:443/web-events/?pretty'
            requests.delete(url=url)

        def renew_index():
            url = "https://5qrfvytdf2:8l2r3918kx@elm-533263851.eu-west-1.bonsaisearch.net/web-events/web-items/_bulk?pretty"
            payload = open('items.json',encoding='utf-8',errors='ignore')
            print(payload)
            headers = {'content-type' : 'application/json'}
            requests.post(url=url,data=payload.read().encode("utf-8"),headers=headers)


        delete_index()
        renew_index()


