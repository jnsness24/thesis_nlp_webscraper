# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy



class Item(scrapy.Item):
    # text Fieldss
    h1_tags = scrapy.Field()
    h2_tags = scrapy.Field()
    h3_tags = scrapy.Field()
    h4_tags = scrapy.Field()
    h5_tags = scrapy.Field()
    h6_tags = scrapy.Field()
    #a_tags = scrapy.Field()
    article_tags = scrapy.Field()
    span_tags = scrapy.Field()
    p_tags = scrapy.Field()

    # Housekeeping
    language = scrapy.Field()
    project = scrapy.Field()
    spider = scrapy.Field()
    server = scrapy.Field()
    date = scrapy.Field()
    url = scrapy.Field()

    # Dict Parents Fields
    meta =  scrapy.Field()
    texts =  scrapy.Field()

    # NLP Fields
    source_texts = scrapy.Field()
    cleaned_texts = scrapy.Field()
    enriched_dep_texts = scrapy.Field()
    enriched_dep_subverb_texts = scrapy.Field()
    enriched_pos_texts = scrapy.Field()
    enriched_ner_texts = scrapy.Field()
    noun_chunks = scrapy.Field()

    pass




