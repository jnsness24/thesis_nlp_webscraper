import en_core_web_sm as en
import de_core_news_sm as de
import json
from googletrans import Translator

"""
Only a single Tool for processing verbs (technical verbs or stateinfluencing verbs) to the same form as sources from websites
"""

with open ("stateinfluencing_verbs_raw.json", encoding='utf8') as f:
    data = json.load(f)

english_list = []
german_list = []

nlp = de.load()
for item in data["de_verbs"]:
    for token in nlp(item).doc:
        german_list.append(token.lemma_)

nlp = en.load()
#if no english verbs are given - translate german to english
if len(english_list) == 0:
    translater = Translator()
    list = []
    for word in german_list:
        translated_word = translater.translate(word, dest='en')
        # delete the infinitive form "to ..."
        if translated_word.text.startswith('to ') and len(translated_word.text) > 4:
            list.append((translated_word.text[3:]))
        else:
            list.append(translated_word.text)
    for item in list:
        for token in nlp(item).doc:
            english_list.append((token.lemma_))

else:
    for item in data["en_verbs"]:
        for token in nlp(item).doc:
            english_list.append(token.lemma_)




lemmatized_json = {}
lemmatized_list = english_list + german_list
lemmatized_json["verbs"] = set(lemmatized_list)

# with open("technical_verbs_lemmatized.json", 'w') as f:
#     json.dump(lemmatized_json,f,default=str, ensure_ascii=False)

with open("stateinfluencing_verbs_lemmatized.json", 'w', encoding='utf8') as f:
    json.dump(lemmatized_json,f,default=str, ensure_ascii=False)


