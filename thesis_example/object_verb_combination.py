import thesis_example.wordslist_cache as wl

class Object_verb_combination:
    def __init__(self, nlp_doc):
        self.nlp_doc = nlp_doc
        self.sv_comb = None
        self.state_influencing = False


    def _attach_word(self, word):
        self.sv_comb += word + " "
        pass


    def create_verb_object_combination(self):
        """
        Fraunhofer's logic for finding hints to technologies
        If a Verb is in the list of stateinfluencing Verbs - the whole sentence or unit is marked
        after that, every Verb, Noun and Proper Noun is saved for later research
        :return:
        """
        stash_list = []
        for token in self.nlp_doc:
            if token.pos_ in ["VERB", "NOUN", "PROPN"]:
                stash_list.append(token.text)
            if token.lemma_ in wl.list and token.pos_ == "VERB":
                # change type of sv_comb from None to String
                self.sv_comb = str()
                self.state_influencing = True

        if self.state_influencing == True:
            for token in stash_list:
                self._attach_word(token)
            print(stash_list)


                #
                # """
                # if not token.head.text == token.text:
                #     self._attach_word(token.head.text)
                #
                # # Left side of children words
                # for subtoken in token.lefts:
                #     if not subtoken.is_punct:
                #         self._attach_word(subtoken.text)
                #
                # self._attach_word(token.lemma_)
                #
                # for subtoken in token.rights:
                #     if not subtoken.is_punct:
                #         self._attach_word(subtoken.text)
                #
                # """









