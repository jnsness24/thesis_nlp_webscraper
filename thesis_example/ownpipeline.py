import json
import thesis_example.nlp_doc as nlpdoc
import thesis_example.object_verb_combination as sv


import codecs
import time
import os

class JsonWriterPipeline(object):
    """
    Pipeline for Item Processing
    JSON File is opened at start of pipeline and closed at end of pipeline
    NLP tasks are processed in between
    """

    def open_spider(self,spider):

        self.file = codecs.open('items.json', 'w', encoding='utf-8')

    def close_spider(self,spider):
        self.file.close()


    def process_item(self,item,spider):
        # Need that line for every item in the finished file for _bulk API
        index_dict = {"index" : {}}
        lang = item['meta']['language']

        start = time.clock()

        # Create a Spacy Document with the proper language Pack
        nlp_doc = nlpdoc.NLP_doc(lang, item['texts']['source_texts'])
        nlp_doc.create_NLP_docs()

        # Prepare empty lists for prompt filling
        pos_list = []
        ner_list = []
        dep_list = []
        cleaned_string = []
        noun_chunks = []

        for sentence in nlp_doc.docs:
            pos_dict = {}
            ner_dict = {}
            dep_dict = {}

            # add the POS and DEP information and add them to the respective dictionary
            for token in sentence.doc:
                pos_dict[token.pos_] = token.text
                dep_dict[token.dep_] = token.text

            # add the NOUN Chunk information and add them to the respective dictionary
            # Noun Chunks = Nouns and "what goes on with them"
            for chunk in sentence.noun_chunks:
                noun_chunks.append(chunk.root.text)


            # Creates a Subject - Verb combination, ideal constellation for technical verbs
            subverb = sv.Object_verb_combination(sentence.doc)
            subverb.create_verb_object_combination()



            #double check if dictionary is valid e.g. is not empty - and fill it
            if pos_dict:
                pos_list.append(pos_dict)

            if dep_dict:
                dep_list.append(dep_dict)

            # do the same with NER information
            for token in sentence.doc.ents:
                ner_dict[token.label_] = token.text
            if ner_dict:
                ner_list.append(ner_dict)


            #do the same with Cleaning (stopwords and punctuation remove - crop token to lemma)
            for token in sentence.doc:
                if not token.is_stop and not token.is_punct and not token.like_num:
                    cleaned_string.append(token.lemma_)




            #add the information under the respective tag
            item['texts']['cleaned_texts'] = " ".join(cleaned_string)
            item['texts']['enriched_dep_texts'] = dep_list
            item['texts']['enriched_dep_subverb_texts'] = subverb.sv_comb
            item['texts']['enriched_pos_texts'] = pos_list
            item['texts']['enriched_ner_texts'] = ner_list
            item['texts']['noun_chunks'] = " ".join(noun_chunks)

            print ("Time Took for one item: {0}".format(time.clock() - start
                                                        ))

        index_line = json.dumps(index_dict) +"\n"
        #ensure utf-8 encoding in json File
        doc_line = json.dumps(dict(item), default=str, ensure_ascii=False) +"\n"

        #todo when file is about 50MB - a single upload is not possible because of 60s request limit. Have to split it
        self.file.write(index_line)
        self.file.write(doc_line)
        return item
