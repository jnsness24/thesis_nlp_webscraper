# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import SitemapSpider, Rule
from scrapy.loader import ItemLoader
from ..items import Item

class SitemapSpider(SitemapSpider):
    name = 'sitemap_spider'
    sitemap_urls = ['http://aoe.com/robots.txt']
    

    def parse(self, response):
        i = Item()
        #h1_css = 'tr#places_country__row td.w2p_fw::text'
        tags_xpath = '//body//text()'
        i['tags'] = response.xpath(tags_xpath).extract()
        return i
