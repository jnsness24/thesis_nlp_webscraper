# -*- coding: utf-8 -*-
import datetime
import socket

import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import SitemapSpider, Rule
from scrapy.loader import ItemLoader
from ..items import Item

class SitemapSpider(SitemapSpider):
    name = 'sitemap_spider'

    sitemap_urls = ['http://aoe.com/robots.txt']
    

    def parse(self, response):

        l = ItemLoader(item=Item(), response=response)
        #l.add_xpath('pTag', '//body//p//text()')
        l.add_xpath('h1Tag', '//body//h1//text()')

        # Housekeeping fields
        #l.add_value('project', self.settings.get('BOT_NAME'))
        #l.add_value('spider', self.name)
        #l.add_value('server', socket.gethostname())
        #l.add_value('date', datetime.datetime.now())

        return l.load_item()
