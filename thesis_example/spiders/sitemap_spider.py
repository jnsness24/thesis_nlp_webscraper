# -*- coding: utf-8 -*-
import datetime
import socket
from nltk import tokenize


from scrapy.spiders import SitemapSpider
from ..items import Item

class SitemapSpider(SitemapSpider):
    """
    Spider for crawling the sitemap of a domain
    just needs link to robots.txt (when sitemap is linked in it) OR directly to sitemap.xml as argument
    """


    #Removed a_tags and span - only useless information
    field_list = ['h1_tags','h2_tags', 'h3_tags', 'h4_tags', 'h5_tags', 'h6_tags', 'article_tags','p_tags']
    name = 'sitemap_spider'

    counter = 0

    #change constructor to accept arguments "-a arg=argument" so that url is configurable from outside
    #Sitemap or robots.txt - scrapy can handle both
    def __init__(self,url=''):
        self.sitemap_urls = [url]
        super().__init__()

    

    def parse(self, response):
        self.counter+=1
        print("\t count single websites: {0}".format(self.counter))

        item = Item()

        # Create Parent-Dict for later ElasticSearch structure
        item['meta'] = {}
        item['texts'] = {}

        item['meta']['url'] = response.url
        item['meta']['project'] = self.settings.get('BOT_NAME')
        item['meta']['spider'] = self.name
        item['meta']['server'] = socket.gethostname()
        item['meta']['date'] = datetime.datetime.now()
        #todo @lang is not working for pages like feralpi - they use <meta name="content-language" content="de" /><meta name="language" content="de" />
        item['meta']['language'] = "".join(response.xpath('@lang').extract())

        # create items that are listed in the field list
        for field in self.field_list:
            # fill the source field with the xpath information
            field_without_tags = field.split("_")[0]
            #get beautiful formatted strings
            filled_tag = remove_tags_and_whitespaces(response.xpath('//body//'+field_without_tags))
            for entry in filled_tag:
                #check if string is valid and long enough for minimum information content
                if entry and len(entry) > 40:
                    # fill the current scrapy item with the information, yield it for further processing in NLP pipeline, and delete it afterwards so that one item is only one division, not a representation of a whole website
                    # Split sentences with nltk sentence Splitter
                    splitted_sentences = tokenize.sent_tokenize(entry)
                    for sentence in splitted_sentences:
                        # eliminates wrong splitted sentences like P. | Hd. and only keeps "long" sentences
                        if len(sentence) < 20:
                            break
                        item['texts'] = {}
                        item['meta']['tag'] = field_without_tags
                        item['texts']['source_texts'] = sentence
                        yield item
                        del item['texts']



def remove_tags_and_whitespaces(xPath_list):
    """
    deletes whitespaces and retrieves pretty plain text
    :param xPath_list:
    :return: cleaned tagList
    """
    tagList = []
    for i in xPath_list:
        cleaned_text = i.xpath('normalize-space()').extract()
        tagList.append(cleaned_text[0])
    return tagList








