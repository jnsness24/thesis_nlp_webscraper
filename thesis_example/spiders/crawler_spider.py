# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from ..items import Item
from urllib.parse import  urlparse
import datetime
import socket
from nltk import tokenize

class CrawlerSpiderSpider(CrawlSpider):
    """
    Spider for crawling from link to link - just needs a homepage as argument and (optional) rules for sites
    """

    # Removed a_tags and span - only useless information
    field_list = ['h1_tags', 'h2_tags', 'h3_tags', 'h4_tags', 'h5_tags', 'h6_tags', 'article_tags', 'p_tags']
    name = 'crawler_spider'
    counter = 0

    def __init__(self,url='',rule='',allowed_domain=''):
        super(CrawlerSpiderSpider,self).__init__()
        # With this method, url is passable as an argument from command line
        parsed_uri = urlparse(url)
        self.start_urls = [url]
        # url http://example.de will be added to allowed domains (only with netloc, example.de) so that crawler does not leave domain
        self.allowed_domains = [parsed_uri.netloc]

        # Rules like "only /service path" can be passed as argument aswell. After resetting them in __init they must be compiled again
        CrawlerSpiderSpider.rules = (Rule(LinkExtractor(allow=r'{}/'.format(rule)), callback='parse_item', follow=True),)
        super(CrawlerSpiderSpider,self)._compile_rules()


    def parse_item(self, response):
        # i = {}
        # print(response.url)
        # #i['domain_id'] = response.xpath('//input[@id="sid"]/@value').extract()
        # #i['name'] = response.xpath('//div[@id="name"]').extract()
        # #i['description'] = response.xpath('//div[@id="description"]').extract()
        # return i

        self.counter += 1
        print("\t count single websites: {0}".format(self.counter))

        item = Item()

        # Create Parent-Dict for later ElasticSearch structure
        item['meta'] = {}
        item['texts'] = {}

        item['meta']['url'] = response.url
        item['meta']['project'] = self.settings.get('BOT_NAME')
        item['meta']['spider'] = self.name
        item['meta']['server'] = socket.gethostname()
        item['meta']['date'] = datetime.datetime.now()
        #If language Tag is not present - it will be tried to guess afterwords
        item['meta']['language'] = "".join(response.xpath('@lang').extract())

        # create items that are listed in the field list
        for field in self.field_list:
            # fill the source field with the xpath information
            field_without_tags = field.split("_")[0]
            # get beautiful formatted strings
            filled_tag = remove_tags_and_whitespaces(response.xpath('//body//' + field_without_tags))
            for entry in filled_tag:
                # check if string is valid and long enough for minimum information content
                if entry and len(entry) > 40:
                    # fill the current scrapy item with the information, yield it for further processing in NLP pipeline, and delete it afterwards so that one item is only one division, not a representation of a whole website
                    # Split sentences with nltk sentence Splitter
                    splitted_sentences = tokenize.sent_tokenize(entry)
                    for sentence in splitted_sentences:
                        # eliminates wrong splitted sentences like P. | Hd. and only keeps "long" sentences
                        if len(sentence) < 20:
                            break
                        item['texts'] = {}
                        item['meta']['tag'] = field_without_tags
                        item['texts']['source_texts'] = sentence
                        #throw it in pipeline for NLP magic
                        yield item
                        del item['texts']


def remove_tags_and_whitespaces(xPath_list):
    """
    deletes whitespaces and retrieves pretty plain text
    :param xPath_list:
    :return:
    """
    tagList = []
    for i in xPath_list:
        cleaned_text = i.xpath('normalize-space()').extract()
        tagList.append(cleaned_text[0])
    return tagList