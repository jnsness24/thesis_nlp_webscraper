import json

"""
only single "cache" for reducing read/write operations 
list will be stored in memory - so that comparison has not been done on harddisk
"""

with open("stateinfluencing_verbs_lemmatized.json", "r") as f:
    data = json.load(f)

list = data["verbs"]
