import en_core_web_sm as en
import de_core_news_sm as de
import langdetect

from nltk import tokenize

class NLP_doc:
    def __init__(self, lang, text):
        self.lang = lang
        self.text = text


    def create_NLP_docs(self):
        self.docs = []

        #Check the html tag "language" for the correct spacy language - currently only DE and US is supported
        if self.lang == 'de' or self.lang == 'de-DE':
            #TODO currently Spacy v.2.0's performance ist 10x slower in little corpus (like we have) - has to be upraded (nightly has already a fix)
            nlp = de.load()

        else:
            if self.lang == 'en' or self.lang == 'en-US':
                nlp = en.load()
            else:
                # If de, de-DE, en or en-US is not detectable, nltk tries to guess the language. But this is tricky with only one sentence
                print("Language not detected!")
                lang = langdetect(self.text)
                if lang == 'en':
                    print("langdetect found English!")
                    nlp = en.load()
                else:
                    if lang == 'de':
                        print("langdetect found German!")
                        nlp = de.load()
                    else:
                        # if nltk is not able to find anything, then item is not analyzable
                        print("langdetect found nothing!")
                        self.tag = None
                        self.lang = None

        #Double Check if string is valid e.g. not empty and append it to resultlist
        if self.text:
            self.docs.append(nlp(self.text))
